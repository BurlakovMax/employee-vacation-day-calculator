# Employee vacation day calculator

# How to run app:

##You can use this docker image so run:

````
docker-compose up -d
````

After all docker containers will be up, you need execute next commands

````
docker exec -it php ./bin/setup-dev.sh
````

This command will install all dependencies by composer,
create database and load user data to it.

### And now you can use console commands to get employees vacation days 

````
docker exec -it php php ./bin/console app:vacation-calculate
````

It will ask you about year of calculation, enter it (by default it current year).
And you will see all calculation + there will be file VacationCalculate.txt


## If you prefer to use your own server (PHP 7.2+ and Database)

Simply copy all /src folder to your docroot, config .env file to your Database

````
DATABASE_URL=mysql://root:root@mysql/otivo
````

And run

````
./bin/setup-dev.sh
````    

