<?php

namespace App\Command;

use App\Entity\Employee;
use App\VacationDaysCalculator\VacationDaysCalculator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class VacationCalculateCommand extends Command
{
    protected static $defaultName = 'app:vacation-calculate';
    protected $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Employee::class);
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $currentYear = new \DateTime('now');
        $filesystem = new Filesystem();

        $year = $io->askQuestion(new Question('Input year for calculation in YYYY format', $currentYear->format('Y')));

        if (!is_numeric($year) || strlen($year) < 4) {
            $io->error('Year must be a number in format YYYY');

            return;
        }

        $filesystem->dumpFile('VacationCalculate.txt', '');

        $employees = $this->repository->findAll();

        foreach ($employees as $employee) {
            $io->writeln('=========');

            $calculator = new VacationDaysCalculator($employee, $year);
            $days = $calculator->calculateVacationsDays();

            $io->writeln($employee->getName().' have a '.$days.' days');
            $filesystem->appendToFile('VacationCalculate.txt', $employee->getName().' have a '.$days.' days '."\n");

            $io->writeln('=========');
        }

        $io->success('All data has been written to the file - VacationCalculate.txt');
    }
}
