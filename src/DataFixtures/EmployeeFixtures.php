<?php

namespace App\DataFixtures;

use App\Entity\Employee;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EmployeeFixtures extends Fixture
{
    const EMPLOYEE_LIST = [
      ['name' => 'Hans Müller', 'birthDay' => '30.12.1950', 'startDate' => '01.01.2001', 'special' => null],
      ['name' => 'Angelika Fringe', 'birthDay' => '09.06.1966 ', 'startDate' => '15.01.2001', 'special' => null],
      ['name' => 'Peter Klever', 'birthDay' => '12.07.1991', 'startDate' => '15.05.2016', 'special' => 27],
      ['name' => 'Marina Helter', 'birthDay' => '26.01.1970', 'startDate' => '15.01.2018', 'special' => null],
      ['name' => 'Sepp Meier', 'birthDay' => '23.05.1980', 'startDate' => '01.12.2017', 'special' => null],
      ['name' => 'Maksim Burlakov', 'birthDay' => '05.10.1985', 'startDate' => '01.10.2019', 'special' => null],
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::EMPLOYEE_LIST as $employee) {
            $emp = new Employee();
            $emp->setName($employee['name']);
            $emp->setDateOfBirth(new \DateTime($employee['birthDay']));
            $emp->setStartDate(new \DateTime($employee['startDate']));
            $emp->setSpecialContract($employee['special']);

            $manager->persist($emp);
        }

        $manager->flush();
    }
}
