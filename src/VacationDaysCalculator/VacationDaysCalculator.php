<?php

namespace App\VacationDaysCalculator;

use App\Entity\Employee;

/**
 * Class VacationDaysCalculator
 *
 * @package App\VacationDaysCalculator
 */
class VacationDaysCalculator
{

    /**
     * Each employee has a minimum of 26 vacation days regardless of age
     */
    const DEFAULT_VACATION_DAYS = 26;

    /**
     * @var \App\Entity\Employee
     */
    private $employee;

    /**
     * @var int
     */
    private $calculationYear;

    /**
     * VacationDaysCalculator constructor.
     *
     * @param \App\Entity\Employee $employee
     * @param int $calculationYear
     */
    public function __construct(Employee $employee, int $calculationYear)
    {
        $this->employee = $employee;
        $this->calculationYear = $calculationYear;
    }

    /**
     * @return int
     */
    public function calculateVacationsDays(): int
    {
        $vacationsDays = $this->employee->getSpecialContract() ?? self::DEFAULT_VACATION_DAYS;
        $vacationsDays = $vacationsDays + $this->getAdditionalDaysByAge();
        $vacationsDays = $vacationsDays * $this->isThisYearStartOfWork();

        return $vacationsDays;
    }

    /**
     * Employee >= 30 years do get one additional vacation day every 5 years
     *
     * @return int
     */
    private function getAdditionalDaysByAge(): int
    {
        $birthYear = $this->employee->getDateOfBirth()->format('Y');
        $age = $this->calculationYear - $birthYear;

        if ($age >= 35) {
            $additionalDays = ($age - 30) / 5;

            return $additionalDays;
        }

        return 0;
    }

    /**
     * Employees starting their work at Ottivo in the course of the year get one-twelfth of the their
     * yearly vacation days starting from the next month's 1st
     *
     * @return float
     */
    private function isThisYearStartOfWork(): float
    {
        $startDate = $this->employee->getStartDate();
        $startYear = $startDate->format('Y');

        if ($startYear > $this->calculationYear) {
            return 0;
        }

        if ($startYear == $this->calculationYear) {
            $month = $startDate->format('n');

            return (12 - $month) / 12;
        }

        return 1;
    }
}
