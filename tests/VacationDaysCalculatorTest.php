<?php

namespace App\Tests;

use App\Entity\Employee;
use App\VacationDaysCalculator\VacationDaysCalculator;
use PHPUnit\Framework\TestCase;

/**
 * Class VacationDaysCalculatorTest.
 */
class VacationDaysCalculatorTest extends TestCase
{

    /**
     * @param $name
     * @param $birthDate
     * @param $startWorkDate
     * @param $special
     * @param $calculationYer
     * @param $expected
     *
     * @dataProvider employeesDataProvider
     *
     * @throws \Exception
     */
    public function testGetAdditionalDaysByAge($name, $birthDate, $startWorkDate, $special, $calculationYer, $expected)
    {
        $employee = new Employee();
        $employee->setName($name);
        $employee->setDateOfBirth(new \DateTime($birthDate));
        $employee->setStartDate(new \DateTime($startWorkDate));
        $employee->setSpecialContract($special);

        $calculator = new VacationDaysCalculator($employee, $calculationYer);

        $this->assertEquals($expected, $calculator->calculateVacationsDays());
    }

    /**
     * @return array
     */
    public function employeesDataProvider()
    {
        return [
          ['Hans Müller', '05-10-1985', '01-01-2018', 30, 2019, 30],
          ['Hans Müller', '09-06-1966', '15-05-2016', 27, 2019, 31],
          ['Hans Müller', '05-10-1991', '15-01-2001', null, 2005, 26],
          ['Hans Müller', '05-01-1985', '01-10-2019', null, 2019, 4],
          ['Hans Müller', '05-03-1975', '01-12-2017', null, 2018, 28],
          ['Hans Müller', '05-07-1965', '01-12-2001', null, 2002, 27],
        ];
    }
}
